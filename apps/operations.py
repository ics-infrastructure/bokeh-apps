
from functools import partial
from tornado import gen
from bokeh.layouts import column,row
from bokeh.plotting import figure, curdoc
from bokeh.layouts import layout, widgetbox
from bokeh.models.widgets import Select, Toggle, Button
from bokeh.models.widgets import Tabs, Panel
from bokeh.layouts import widgetbox
from bokeh.models.widgets import PreText
import epics
import time
import datetime
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import DataTable, DateFormatter, TableColumn, NumberFormatter

doc=curdoc()


class MessageBuffePrPVS:

    """ class that implements a not-yet-full buffer """
    def __init__(self,size_max):
        self.max = size_max
        self.timestamp = []
        self.value =[]
        self.message = []

    class __Full:
        """ class that implements a full buffer """
        def append(self, pv):
            """ Append an element overwriting the oldest one. """
            self.value[self.cur] = pv["value"]
            self.timestamp[self.cur] = pv["timestamp"]
            self.message[self.cur] = datetime.datetime.fromtimestamp(pv["timestamp"]).strftime('%Y-%m-%d %H:%M:%S')+"=>  "+ str(pv["value"])
            self.cur = (self.cur+1) % self.max
        def get(self):
            """ return list of elements in correct order """
            return self.message[self.cur:]+self.message[:self.cur]

    def append(self,pv):
        """append an element at the end of the buffer"""
        self.value.append(pv["value"])
        self.timestamp.append(pv["timestamp"])
        self.message.append( datetime.datetime.fromtimestamp(pv["timestamp"]).strftime('%Y-%m-%d %H:%M:%S')+"=>  "+ str(pv["value"]))
        if len(self.timestamp) == self.max:
            self.cur = 0
            # Permanently change self's class from non-full to full
            self.__class__ = self.__Full

    def get(self):
        """ Return a list of elements from the oldest to the newest. """
        return self.message
  
    def __str__(self):
        m=""
        messagelist=self.get()
        for l in range(len(messagelist)): 
                m=messagelist[l]+'\n'+m
        return m


@gen.coroutine
def update(pv):
    global DictionaryPVs
    global ButtonDictionary
    thePV=pv["pvname"]
    value=pv["value"]
    theButton=ButtonDictionary[thePV]
    if value==0:
        theButton.button_type="danger"
    elif value==1:
        theButton.button_type="success"  

def monPV(**kws):
     thepv=kws
     doc.add_next_tick_callback(partial(update, pv=thepv))

@gen.coroutine
def updateMessage(message):
        pre.text=str(message)

def monMessage(**kws):  
     thepv=kws  
     themessage.append(thepv)
     doc.add_next_tick_callback(partial(updateMessage, message=themessage))

@gen.coroutine
def updateOPI():
    global dataTable
    global data_table
    global PVNameTable

    #update the table
    newValues=[]
    for r in PVNameTable:
#        newValues.append( epics.PV(r).get(use_monitor=False))
        newValues.append( epics.PV(r).get())
    sourceTable.data["Values"]=newValues
    #update the plot
    newISBCMdata=PVIsrc.get(use_monitor=False)
    sourcePlot.data["current"]=newISBCMdata



def monTable(**kws):  
     thepv=kws  
     doc.add_next_tick_callback(partial(updateOPI, pv=thepv))


# Button widget and configure with the call back
PVList=["ISrc-010:ISS-HVPf-Doors:CloseR","ISrc-010:ISS-Intf-HMI:ComIocOK"]
DescriptionList=["Platform Doors","Ground to HMI"]
DictionaryPVs = dict(zip(PVList,  DescriptionList))
ButtonList=[]
thePVList=[]
themessage = MessageBuffePrPVS(20)
i=0
for PVname in PVList:
        thePVList.append(epics.PV(PVname, auto_monitor=True, callback=monPV))
        ButtonList.append( Button(label=DictionaryPVs[PVname]))
ButtonDictionary=dict(zip(PVList,ButtonList))
InterlockPanel = Panel(child=column(ButtonList), title='Interlock')
MessagePV=epics.PV("NSO-LCR:Ops:Msg", auto_monitor=True, callback=monMessage)
pre = PreText(text="Waiting for messages",width=500, height=100)


# Table widget
# Create the data source and the monitor for it

PVNameTable=["ISrc-010:ISS-EVR-Magtr:Event-14-Cnt-I",
     "ISrc-010:TS-EVG-01:Mxc1-Frequency-RB",
     "ISrc-010:ISS-Magtr:PulsHLvlS",
     "ISrc-010:ISS-HVPS:VolR", 
     "ISrc-010:PBI-BCM-001:AI4-IPCH-RBV",
     "LEBT-010:PBI-BCM-001:AI5-IPCH-RBV",
     "LEBT-010:PBI-FC-001:AMC31-AOI11-MaxValueR"
     ]



PVDescriptionTable=["Pulse Counter",
     "Repetion Rate [Hz]",
     "Forward Power [W]", 
     "High Voltage [kV]", 
     "BCM ISrc Charge [uC]",
     "BCM LEBT Charge [uC]",
     "FC max value [mA]"
     ]


PVListTable=[]
ValuesListTable=[]
i=0
for pv in PVNameTable:
    PVListTable.append(epics.PV(PVNameTable[i], auto_monitor=True))
    ValuesListTable.append(PVListTable[i].get())
    i=i+1


dataTable = dict(
        PVNames=PVNameTable,
        Description=PVDescriptionTable,
        Values=ValuesListTable
    )

#PVmonitorTable=epics.PV("ISrc-010:ISS-EVR-Magtr:Event-14-Cnt-I", auto_monitor=True, callback=monTable)
sourceTable = ColumnDataSource(dataTable)

columns = [
        TableColumn(field="Description", title="PV"),
        TableColumn(field="Values", title="Value", formatter=NumberFormatter(format='0.000')),
    ]
data_table = DataTable(source=sourceTable, columns=columns, width=400, height=280)
# Now the ISrc BCM Plot
# create a plot and style its properties
p = figure(x_range=(0, 200), y_range=(0, 100))
p.border_fill_color = "black"
p.background_fill_color = "black"
p.outline_line_color = None
p.grid.grid_line_color = None
PVIsrc=epics.PV("ISrc-010:PBI-BCM-001:AI4-Compressed")
current=PVIsrc.get()
index=list(range(len(current)))

dataPlot = {'index': index, 'current': current}

sourcePlot = ColumnDataSource(data=dataPlot)

p = figure()
p.line(x='index', y='current', source=sourcePlot)

# GUI 
StatusPanel = Panel(child=column(data_table,p, pre), title='Messages')
tabs = Tabs(tabs=[StatusPanel,InterlockPanel])


doc.add_root(tabs)
doc.add_periodic_callback(updateOPI, 1000)

